import React from "react";
import AutoComplete from "./AutoComplete";
import TextHighlight from "./TextHighlight";
import { QueryClient, QueryClientProvider } from "react-query";

const queryClient = new QueryClient();

function App() {
  return (
    <div>
      {/* <TextHighlight /> */}
      <QueryClientProvider client={queryClient}>
        <AutoComplete />
      </QueryClientProvider>
    </div>
  );
}

export default App;
