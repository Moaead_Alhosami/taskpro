import React, { useState, useEffect } from "react";
import { FaSearch } from "react-icons/fa";
import axios from "axios";
import { useQuery } from "react-query";
import { Pagination } from 'antd';
import { motion } from "framer-motion";


const AutoComplete = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [options, setOptions] = useState([]);
  const [filteredOptions, setFilteredOptions] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(10);

  const { data, isLoading, error } = useQuery("options", () =>
    axios.get("https://jsonplaceholder.typicode.com/posts").then((response) =>
      response.data.map((post) => ({
        label: post.title,
        value: post.id.toString(),
      }))
    )
  );

  useEffect(() => {
    if (data) {
      setOptions(data);
    }
  }, [data]);

  useEffect(() => {
    setFilteredOptions(
      options.filter(
        (option) =>
          option.label.toLowerCase().includes(searchTerm.toLowerCase()) ||
          option.value.toLowerCase().includes(searchTerm.toLowerCase())
      )
    );
  }, [searchTerm, options]);

  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = filteredOptions.slice(indexOfFirstPost, indexOfLastPost);

  const paginate = pageNumber => setCurrentPage(pageNumber);

  return (
    <div className="p-16 ">
      <div className="relative">
        <FaSearch className="absolute left-3 top-1/2 transform -translate-y-7" />
        <input
          type="text"
          placeholder="Search For Posts ..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          className="w-full p-2 pl-10 mb-10 border shadow-md border-gray-300 rounded-xl focus:outline-none focus:border-indigo-500"
        />
      </div>
      {currentPosts.map((option, i) => (
        <motion.div
          key={i}
          className="px-5 mb-5 text-lg font-semibold border overflow-hidden shadow-md bg-white border-gray-300 rounded-xl hover:scale-105 transition-all flex items-center gap-10 h-24"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ delay: i * 0.1 }}
        >
          <p>{option.value}</p>
          <h2 className="text-lg">
            {option.label
              .split(new RegExp(`(${searchTerm})`, "gi"))
              .map((part, i) =>
                part.toLowerCase() === searchTerm.toLowerCase() ? (
                  <span key={i} className="bg-yellow-300">
                    {part}
                  </span>
                ) : (
                  part
                )
              )}
          </h2>
        </motion.div>
      ))}
      <Pagination
        total={filteredOptions.length}
        showTotal={(total, range) => `${range[0]}-${range[1]} of ${total} items`}
        defaultPageSize={postsPerPage}
        defaultCurrent={1}
        onChange={paginate}
      />
    </div>
  );
};

export default AutoComplete;