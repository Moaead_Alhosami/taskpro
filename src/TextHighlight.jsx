import React, { useState, useEffect } from "react";

function HighlightText() {
  const originalText =
    "He ordered his regular breakfast. Two eggs sunnyside up, hash browns, and two strips of bacon. He continued to look at the menu wondering if this would be the day he added something new. This was also part of the routine. A few seconds of hesitation to see if something else would be added to the order before demuring and saying that would be all. It was the same exact meal that he had ordered every day for the past two years.";

  const [textToSearch, setTextToSearch] = useState("");
  const [paragraph, setParagraph] = useState(originalText);

  useEffect(() => {
    setParagraph(originalText);
  }, [textToSearch]);

  const search = () => {
    if (textToSearch.trim() !== "") {
      let pattern = new RegExp(`${textToSearch}`, "gi");
      setParagraph(
        paragraph.replace(pattern, (match) => `<mark>${match}</mark>`)
      );
    }
  };

  return (
    <div className="p-6 mt-10 max-w-sm mx-auto bg-white rounded-xl shadow-md flex flex-col gap-16 items-center space-x-4">
      <div className="flex-shrink-0">
        <input
          type="text"
          value={textToSearch}
          onChange={(e) => setTextToSearch(e.target.value)}
          placeholder="Enter text to search.."
          className="shadow appearance-none border rounded py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
        />
        <button onClick={search} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">
          Search
        </button>
      </div>
      <div>
        <text dangerouslySetInnerHTML={{ __html: paragraph }} className="text-gray-700" />
      </div>
    </div>
  );
}

export default HighlightText;
